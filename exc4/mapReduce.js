// *** 3T Software Labs, Studio 3T: MapReduce Job ****

// Variable for db
var __3tsoftwarelabs_db = "hafifa";

// Variable for map
var __3tsoftwarelabs_map = function () {
    this.past_treatments.forEach(treatment => emit(treatment.treatment_type, treatment.cost));
}
;

// Variable for reduce
var __3tsoftwarelabs_reduce = function (key, values) {
    let sum = 0;
    let count = 0;

 for( let i = 0; i < values.length; i++ ){
    sum += values[i]; 
    count++;
}

return {sum: sum, count: count};
}
;

// Variable for finalize
var __3tsoftwarelabs_finalize = function (key, reducedValue) {
	const avg = reducedValue.sum/reducedValue.count;
    return {avg: avg, amount: reducedValue.count};
}
;

db.runCommand({ 
    mapReduce: "Vehicles",
    map: __3tsoftwarelabs_map,
    reduce: __3tsoftwarelabs_reduce,
    finalize: __3tsoftwarelabs_finalize,
    out: {"inline": 1},
    query: {"type.manufacturing_date" : { $gt : ISODate("2000-01-01" ) } },
    sort: {},
    inputDB: "hafifa",
 });
