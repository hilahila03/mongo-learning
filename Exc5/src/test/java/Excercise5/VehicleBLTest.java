package Excercise5;

import Exercise5.VehicleBL;
import Exercise5.VehicleDAL;
import Exercise5.models.*;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VehicleBLTest {
    private static final String NUMBER = "12345678";
    private static final String EMPTY_STRING = "";
    public static final String NUMBER1 = "12345687";
    public static final String NUMBER2 = "12345689";
    public static final String NUMBER3 = "12345690";

    @Mock
    VehicleDAL vehicleDal;

    @InjectMocks
    VehicleBL testedVehicleBl;

    @Test
    public void findByNumber_whenNumberIsValid_thenCallGetByNumber() {
        testedVehicleBl.findVehicleByNumber(NUMBER);
        verify(vehicleDal, times(1)).getByNumber(NUMBER);
    }

    @Test
    public void findByNumber_whenNumberIsInvalid_thenDontCallGetByNumber() {
        final String invalidString = "178";
        testedVehicleBl.findVehicleByNumber(invalidString);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void findByNumber_whenNumberIsEmpty_thenDontCallGetByNumber() {
        testedVehicleBl.findVehicleByNumber(EMPTY_STRING);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void findByStatusAndManufacturer_whenFieldsAreValid_thenCallGetByStatusAndManufacturer() {
        final Status status = Status.IN_MISSION;
        final String manufacturer = "Toyota";
        testedVehicleBl.findVehicleByStatusAndManufacturer(status, manufacturer);
        verify(vehicleDal, times(1)).getByStatusAndManufacturer(Status.IN_MISSION, "Toyota");
    }

    @Test
    public void findByStatusAndManufacturer_whenStatusIsNull_thenDontCallGetByStatusAndManufacturer() {
        final String manufacturer = "Toyota";

        testedVehicleBl.findVehicleByStatusAndManufacturer(null, manufacturer);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void findByStatusAndManufacturer_whenManufacturerIsInValid_thenDontCallGetByStatusAndManufacturer() {
        final Status status = Status.IN_MISSION;

        testedVehicleBl.findVehicleByStatusAndManufacturer(status, EMPTY_STRING);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void findByStatusAndManufacturer_whenManufacturerIsInNull_thenDontCallGetByStatusAndManufacturer() {
        final Status status = Status.IN_MISSION;

        testedVehicleBl.findVehicleByStatusAndManufacturer(status, null);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void addTreatment_whenAllArgsValid_thenAddTreatment() {
        final Treatment treatment = Treatment.builder()
                .date(new Date())
                .treatmentType(TreatmentType.BRAKES_REPLACEMENT)
                .cost(6)
                .build();

        final UpdateActionResult mockedResult = new UpdateActionResult(1, 1);
        Mockito.when(vehicleDal.addTreatment(NUMBER, treatment))
                .thenReturn(mockedResult);
        final Optional<UpdateActionResult> result = testedVehicleBl.addTreatment(NUMBER, treatment);
        verify(vehicleDal, times(1))
                .addTreatment(NUMBER, treatment);
        assertTrue(result.isPresent());
    }

    @Test
    public void addTreatment_whenIdIsInValid_thenDontCallAddTreatment() {
        final Treatment treatment = Treatment.builder()
                .date(new Date())
                .treatmentType(TreatmentType.BRAKES_REPLACEMENT)
                .cost(6)
                .build();

        testedVehicleBl.addTreatment(EMPTY_STRING, treatment);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void addTreatment_whenTreatmentIsNull_thenDontCallAddTreatment() {
        testedVehicleBl.addTreatment(NUMBER, null);
        verifyNoInteractions(vehicleDal);
    }

    @Test
    public void addTreatment_whenAllArgsAreNull_thenDontCallAddTreatment() {
        testedVehicleBl.addTreatment(null, null);
        verifyNoInteractions(vehicleDal);
    }

    @Test()
    public void addVehicle_whenVehicleIsValid_thenAddVehicle() {
        final Vehicle mockedVehicle = getMockVehicle(NUMBER);
        testedVehicleBl.addVehicle(mockedVehicle);
        verify(vehicleDal, times(1)).add(mockedVehicle);
    }

    @Test
    public void addVehicle_whenVehicleIsNull_thenDontAddVehicle() {
        try {
            testedVehicleBl.addVehicle(null);
            fail("should throw Exception!");
        } catch (Exception e) {
            verifyNoInteractions(vehicleDal);
            throw e; // TODO add expected exception
        }
    }

    @Test()
    public void addVehicles_whenVehiclesAreValid_ThenAddVehicles() {
        final List<Vehicle> vehicles = Arrays.asList(
                getMockVehicle(NUMBER1),
                getMockVehicle(NUMBER2),
                getMockVehicle(NUMBER3)
        );

        testedVehicleBl.addVehicles(vehicles);
        verify(vehicleDal, times(1))
                .add(vehicles);
    }

    @Test
    public void addVehicles_whenVehiclesCollectionIsNull_thenDontAddVehicles() {
        try {
            testedVehicleBl.addVehicles(null);
            fail("should throw Exception!");
        } catch (Exception e) {
            verifyNoInteractions(vehicleDal);
        }
    }

    // Todo add A
    @Test
    public void addVehicles_whenOneVehicleIsNull() {
        final List<Vehicle> vehicles = Arrays.asList(
                getMockVehicle(NUMBER),
                null,
                getMockVehicle(NUMBER1));
        try {
            testedVehicleBl.addVehicles(vehicles);
            fail("should throw Exception!");
        } catch (Exception e) {
            verifyNoInteractions(vehicleDal);
        }
    }

    @Test()
    public void updateStatus_whenStatusIsValid_thenCallUpdateStatus() {
        testedVehicleBl.updateStatus(NUMBER, Status.IN_MISSION);
        verify(vehicleDal, times(1))
                .updateStatus(NUMBER, Status.IN_MISSION);
    }

    @Test()
    public void updateStatus_whenStatusIsNull_thenDontCallUpdateStatus() {
        testedVehicleBl.updateStatus(NUMBER, null);
        verifyNoInteractions(vehicleDal);
    }

    @Test()
    public void updateStatus_whenIdIsInValid_thenDontCallUpdateStatus() {
        testedVehicleBl.updateStatus(EMPTY_STRING, Status.IN_MISSION);
        verifyNoInteractions(vehicleDal);
    }

    @Test()
    public void updateStatus_whenIdIsNull_thenDontCallUpdateStatus() {
        testedVehicleBl.updateStatus(null, Status.IN_MISSION);
        verifyNoInteractions(vehicleDal);
    }

    @Test()
    public void setAllVehiclesStatus_whenStatusIsValid_thenCallUpdateStatusForAllVehicles() {
        testedVehicleBl.setAllVehiclesStatus(Status.IN_MISSION);
        verify(vehicleDal, times(1))
                .updateStatusForAll(Status.IN_MISSION);
    }

    @Test()
    public void setAllVehiclesStatus_whenStatusIsNull_thenDontCallUpdateStatus() {
        testedVehicleBl.setAllVehiclesStatus(null);
        verifyNoInteractions(vehicleDal);
    }

    public static Vehicle getMockVehicle(String number) {
        final Double[] coordinates = {30.0, 30.0};
        final Point location = new Point(new Position(Arrays.asList(coordinates)));
        final List<Treatment> treatments = Arrays.asList(
                Treatment.builder()
                        .date(new Date())
                        .treatmentType(TreatmentType.BRAKES_REPLACEMENT)
                        .cost(20)
                        .build(),
                Treatment.builder()
                        .date(new Date())
                        .treatmentType(TreatmentType.TYRES_REPLACEMENT)
                        .cost(50)
                        .build()
        );

        return Vehicle.builder()
                .number(number)
                .currentLocation(location)
                .type(Type.builder().manufacturingDate(new Date()).manufacturer("Toyota").build())
                .status(Status.DONE)
                .fuelPercent(40.0)
                .kilometers(15.0)
                .pastTreatments(treatments)
                .build();
    }
}
