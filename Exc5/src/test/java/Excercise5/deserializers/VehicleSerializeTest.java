package Excercise5.deserializers;

import Exercise5.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.junit.Test;
import org.mongojack.internal.MongoJackModule;

import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class VehicleSerializeTest {
    private Date getDateFromString(String dateString) {
        final DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        final TemporalAccessor accessor = timeFormatter.parse(dateString);

        return Date.from(Instant.from(accessor));
    }

    @Test
    public void serializeVehicle_whenVehicleFieldsAreValid_shouldSerialize() {
        final Double[] coordinates = {30.0, 30.0};
        final Treatment[] treatments = {
                Treatment.builder()
                        .cost(20)
                        .treatmentType(TreatmentType.BRAKES_REPLACEMENT)
                        .date(getDateFromString("2020-05-18T07:15:30.643Z"))
                        .build(),
                Treatment.builder()
                        .cost(50)
                        .treatmentType(TreatmentType.TYRES_REPLACEMENT)
                        .date(getDateFromString("2020-05-18T07:15:30.644Z"))
                        .build()
        };

        final Vehicle newVehicle = Vehicle.builder()
                ._id("5ec23612fc5a8f1add215fd1")
                .number("1935830")
                .type(
                        Type.builder()
                                .manufacturingDate(getDateFromString("2020-05-18T07:15:30.651Z"))
                                .manufacturer("Toyota")
                                .build())
                .status(Status.IN_MISSION)
                .currentLocation(new Point(new Position(Arrays.asList(coordinates))))
                .kilometers(40.0)
                .pastTreatments(Arrays.asList(treatments))
                .fuelPercent(40.0)
                .build();

        final String vehicleJsonString = "{\"_id\":{\"$oid\":\"5ec23612fc5a8f1add215fd1\"},\"number\":\"1935830\",\"type\":{\"manufacturing_date\":{\"$date\":\"2020-05-18T07:15:30.651Z\"},\"manufacturer\":\"Toyota\"},\"status\":\"in_mission\",\"currentLocation\":{\"type\":\"Point\",\"coordinates\":[30,30]},\"kilometers\":40,\"pastTreatments\":[{\"date\":{\"$date\":\"2020-05-18T07:15:30.643Z\"},\"treatment_type\":\"brakesReplacement\",\"cost\":20},{\"date\":{\"$date\":\"2020-05-18T07:15:30.644Z\"},\"treatment_type\":\"tyresReplacement\",\"cost\":50}],\"fuelPercent\":40}";

        final ObjectMapper mapper = new ObjectMapper();
        MongoJackModule.configure(mapper);

        try {
            final String serializedVehicle = mapper.writeValueAsString(newVehicle);
            assertEquals(serializedVehicle, vehicleJsonString);
        } catch (IOException e) {
            fail("failed to deserialize");
        }
    }

}
