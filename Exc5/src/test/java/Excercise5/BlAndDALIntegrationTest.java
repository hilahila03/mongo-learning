package Excercise5;


import Exercise5.VehicleBL;
import Exercise5.VehicleDAL;
import Exercise5.models.*;
import com.mongodb.MongoWriteException;
import com.mongodb.client.*;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mongojack.JacksonMongoCollection;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.Assert.*;

public class BlAndDALIntegrationTest {

    private final static MongoClient client = MongoClients.create();
    private static final String NUMBER = "12346578";
    private static final String SECOND_VEHICLE = "13245679";
    private static final String THIRD_VEHICLE = "12345680";
    private static final String NUMBER_FIELD = "number";
    private static final String STATUS_FIELD = "status";

    private final static MongoCollection<Vehicle> collection =
            JacksonMongoCollection.builder().build(client,
                    "Integration",
                    "Vehicles",
                    Vehicle.class,
                    UuidRepresentation.JAVA_LEGACY);

    VehicleBL testedVehicleBl = new VehicleBL(new VehicleDAL(collection));

    @Before
    @After
    public void setUp() {
        collection.deleteMany(new Document());
    }

    public void insertVehicleToDb(String number) {
        final Vehicle newVehicle = VehicleBLTest.getMockVehicle(number);
        collection.insertOne(newVehicle);
    }

    public List<Vehicle> getAsList(FindIterable<Vehicle> findIterable) {
        return StreamSupport.stream(
                findIterable.spliterator(),
                false)
                .collect(Collectors.toList());
    }

    @Test
    public void addVehicle_whenVehicleIsValid_ShouldAddToDB() {
        final Vehicle mockVehicle = VehicleBLTest.getMockVehicle(NUMBER);

        try {
            testedVehicleBl.addVehicle(mockVehicle);
        } catch (MongoWriteException e) {
            fail("failed to add to DB");
        }

        final Optional<String> maybeVehicle_id = Optional.of(collection.find())
                .map(MongoIterable::first)
                .map(Vehicle::get_id);
        assertTrue(maybeVehicle_id.isPresent());
        assertEquals((mockVehicle).get_id(), maybeVehicle_id.get());
    }

    @Test
    public void addVehicle_whenVehicleIsInValid_ShouldNotAddToDB() {
        try {
            testedVehicleBl.addVehicle(null);
        } catch (MongoWriteException e) {
            fail("failed to add to DB");
        }

        final Optional<String> maybeVehicleNumber = Optional.of(collection.find())
                .map(MongoIterable::first)
                .map(Vehicle::getNumber);

        assertFalse(maybeVehicleNumber.isPresent());
    }

    @Test
    public void addVehicles_whenVehiclesAreValid_ShouldAddToDB() {
        final Vehicle firstVehicle = VehicleBLTest.getMockVehicle(NUMBER);
        final Vehicle secondVehicle = VehicleBLTest.getMockVehicle(SECOND_VEHICLE);
        final Vehicle[] vehicles = {firstVehicle, secondVehicle};
        final List<Vehicle> vehiclesToInsert = Arrays.asList(vehicles);
        final int inserted = testedVehicleBl.addVehicles(vehiclesToInsert);

        assertEquals(2, inserted);
        final List<Vehicle> vehiclesInDB = getAsList(collection.find());
        assertEquals(2, vehiclesInDB.size());
    }

    @Test
    public void addVehicles_whenOneVehicleIsInvalid_ShouldNotAddToDB() {
        final Vehicle firstVehicle = VehicleBLTest.getMockVehicle(SECOND_VEHICLE);
        final Vehicle[] vehicles = {firstVehicle, null};
        final List<Vehicle> vehiclesToInsert = Arrays.asList(vehicles);
        final int inserted = testedVehicleBl.addVehicles(vehiclesToInsert);

        assertEquals(inserted, 0);
        final List<Vehicle> vehiclesInDB = getAsList(collection.find());
        assertEquals(0, vehiclesInDB.size());
    }

    @Test
    public void addVehicles_whenVehiclesArrayIsNull_ShouldNotAddToDB() {
        int inserted = testedVehicleBl.addVehicles(null);

        assertEquals(inserted, 0);
        final List<Vehicle> vehiclesInDB = getAsList(collection.find());
        assertEquals(0, vehiclesInDB.size());
    }

    @Test
    public void addVehicles_whenOneVehicleAlreadyExists_ShouldAddToDBTheOther() {
        final Vehicle firstVehicle = VehicleBLTest.getMockVehicle(NUMBER);
        final Vehicle secondVehicle = VehicleBLTest.getMockVehicle(SECOND_VEHICLE);
        insertVehicleToDb(SECOND_VEHICLE);

        final List<Vehicle> vehiclesToInsert = Arrays.asList(firstVehicle, secondVehicle);
        int inserted = testedVehicleBl.addVehicles(vehiclesToInsert);

        assertEquals(1, inserted);

        final List<Vehicle> vehicleNumber = getAsList(collection.find());
        assertEquals(2, vehicleNumber.size());
    }

    @Test
    public void findByNumber_whenVehicleExists_ShouldFindVehicle() {
        final String newVehicleNumber = NUMBER;
        insertVehicleToDb(newVehicleNumber);

        final Optional<Vehicle> maybeVehicle = testedVehicleBl.findVehicleByNumber(newVehicleNumber);
        assertTrue(maybeVehicle.isPresent());
        assertEquals(newVehicleNumber, maybeVehicle.get().getNumber());
    }

    @Test
    public void findByNumber_whenVehicleDoesNotExist_ShouldNotFindVehicle() {
        final Optional<Vehicle> maybeVehicle = testedVehicleBl.findVehicleByNumber(NUMBER);
        assertFalse(maybeVehicle.isPresent());
    }

    @Test
    public void findByNumber_whenNumberIsInvalid_ShouldNotFindVehicle() {
        final String invalidNumber = "13dfsfs";
        final Optional<Vehicle> maybeVehicle = testedVehicleBl.findVehicleByNumber(invalidNumber);
        assertFalse(maybeVehicle.isPresent());
    }

    @Test
    public void findByStatusAndManufacturer_whenVehicleExists_ShouldFindVehicles() {
        final String manufacturer = "hila";
        final Status status = Status.DONE;
        final Vehicle firstVehicle = VehicleBLTest.getMockVehicle(NUMBER);
        final Vehicle secondVehicle = VehicleBLTest.getMockVehicle(SECOND_VEHICLE);
        final Vehicle[] vehicles = {firstVehicle, secondVehicle};
        final List<Vehicle> vehiclesToInsert = Arrays.asList(vehicles);
        vehiclesToInsert.forEach(ron -> {
            ron.setStatus(status);
            ron.setType(Type.builder().manufacturingDate(new Date()).manufacturer(manufacturer).build());
        });

        collection.insertMany(vehiclesToInsert);

        final Optional<List<Vehicle>> maybeFoundVehicles = testedVehicleBl.findVehicleByStatusAndManufacturer(status, manufacturer);
        assertTrue(maybeFoundVehicles.isPresent());
        assertEquals(maybeFoundVehicles.get().size(), vehiclesToInsert.size());
        IntStream.range(0, vehiclesToInsert.size()).forEach(i ->
                assertEquals(maybeFoundVehicles.get().get(i).get_id(),
                        vehiclesToInsert.get(i).get_id()));
    }

    @Test
    public void findByStatusAndManufacturer_whenVehicleNotExists_ShouldReturnEmptyList() {
        final String manufacturer = "hila";
        final Status status = Status.DONE;

        final Optional<List<Vehicle>> maybeFoundVehicles = testedVehicleBl.findVehicleByStatusAndManufacturer(status, manufacturer);
        assertTrue(maybeFoundVehicles.isPresent());
        assertEquals(maybeFoundVehicles.get().size(), 0);
    }

    @Test
    public void findByStatusAndManufacturer_whenInputIsInvalid_ShouldReturnEmptyOptional() {
        final Optional<List<Vehicle>> maybeFoundVehicles = testedVehicleBl.findVehicleByStatusAndManufacturer(null, "");
        assertFalse(maybeFoundVehicles.isPresent());
    }

    @Test
    public void addTreatment_whenVehicleExists_ShouldAddTreatment() {
        final String newVehicleNumber = NUMBER;
        insertVehicleToDb(newVehicleNumber);
        final Treatment treatment = Treatment.builder()
                .cost(20)
                .treatmentType(TreatmentType.BRAKES_REPLACEMENT)
                .date(new Date())
                .build();

        final Optional<UpdateActionResult> maybeUpdateResult = testedVehicleBl.addTreatment(newVehicleNumber, treatment);
        assertTrue(maybeUpdateResult.isPresent());
        assertEquals(maybeUpdateResult.get().getUpdated(), 1);
        assertEquals(maybeUpdateResult.get().getMatched(), 1);

        final Optional<List<Treatment>> maybeUpdatedVehicleTreatment = Optional.ofNullable(
                collection.find(eq(NUMBER_FIELD, newVehicleNumber)).first())
                .map(Vehicle::getPastTreatments);
        ;
        assertTrue(maybeUpdatedVehicleTreatment.isPresent());
        assertTrue(maybeUpdatedVehicleTreatment.get().contains(treatment));
    }

    @Test
    public void addTreatment_whenVehicleDoesNotExist_ShouldNotAddTreatment() {
        final String newVehicleNumber = NUMBER;
        insertVehicleToDb(newVehicleNumber);

        final Optional<UpdateActionResult> maybeUpdateResult = testedVehicleBl.addTreatment(newVehicleNumber, null);
        assertFalse(maybeUpdateResult.isPresent());
    }

    @Test
    public void addTreatment_whenVehicleNumberIsInvalid_ShouldNotAddTreatment() {
        final String emptyVehicleNumber = "";
        final Treatment treatment = Treatment.builder()
                .cost(20)
                .treatmentType(TreatmentType.BRAKES_REPLACEMENT)
                .date(new Date())
                .build();

        final Optional<UpdateActionResult> maybeUpdateResult = testedVehicleBl.addTreatment(emptyVehicleNumber, treatment);
        assertFalse(maybeUpdateResult.isPresent());
    }

    @Test
    public void updateStatus_whenStatusAndIdAreValid_ShouldAddStatus() {
        insertVehicleToDb(NUMBER);
        final Status firstStatus = Status.IN_MISSION;

        final Optional<UpdateActionResult> maybeResult = testedVehicleBl.updateStatus(NUMBER, firstStatus);

        assertTrue(maybeResult.isPresent());
        assertEquals(1, maybeResult.get().getMatched());
        assertEquals(1, maybeResult.get().getUpdated());

        final Vehicle updatedVehicle = collection.find(eq(NUMBER_FIELD, NUMBER)).first();
        final Optional<Vehicle> maybeUpdatedVehicle = Optional.ofNullable(updatedVehicle);
        maybeUpdatedVehicle.ifPresentOrElse(
                v -> assertEquals(v.getStatus(), firstStatus),
                () -> fail("updated vehicle does not exist"));
    }

    @Test
    public void updateStatus_whenVehicleDoesNotExist_ShouldNotUpdateStatus() {
        final Status firstStatus = Status.DONE;
        final Optional<UpdateActionResult> maybeResult = testedVehicleBl.updateStatus(NUMBER, firstStatus);

        assertTrue(maybeResult.isPresent());
        assertEquals(0, maybeResult.get().getMatched());
        assertEquals(0, maybeResult.get().getUpdated());

        final Vehicle vehicle = collection.find(eq(NUMBER_FIELD, NUMBER)).first();
        final Optional<Vehicle> maybeUpdatedVehicle = Optional.ofNullable(vehicle);
        assertFalse(maybeUpdatedVehicle.isPresent());
    }

    @Test
    public void updateStatus_whenIdIsInvalid_ShouldNotUpdateStatus() {
        final String vehicleEmptyNumber = "";
        final Status firstStatus = Status.DONE;

        final Optional<UpdateActionResult> maybeResult = testedVehicleBl.updateStatus(vehicleEmptyNumber, firstStatus);

        assertFalse(maybeResult.isPresent());
    }

    @Test
    public void updateStatus_whenStatusIsInvalid_ShouldNotUpdateStatus() {
        final Optional<UpdateActionResult> maybeResult = testedVehicleBl.updateStatus(NUMBER, null);

        assertFalse(maybeResult.isPresent());
    }

    @Test
    public void setAllVehiclesStatus_whenMultipleVehiclesExist_ShouldUpdateAll() {
        insertVehicleToDb(NUMBER);
        insertVehicleToDb(SECOND_VEHICLE);
        insertVehicleToDb(THIRD_VEHICLE);
        final Status statusToUpdate = Status.IN_TREATMENT;

        final Optional<Long> maybeResult = testedVehicleBl.setAllVehiclesStatus(statusToUpdate);

        assertTrue(maybeResult.isPresent());
        final long result = maybeResult.get();
        assertEquals(3, result);

        final List<Vehicle> updatedVehicles = getAsList(collection.find(eq(STATUS_FIELD, statusToUpdate)));
        assertEquals(3, updatedVehicles.size());
    }

    @Test
    public void setAllVehiclesStatus_whenOneVehicleExist_ShouldUpdate() {
        insertVehicleToDb(NUMBER);
        final Status statusToUpdate = Status.IN_TREATMENT;

        final Optional<Long> maybeResult = testedVehicleBl.setAllVehiclesStatus(statusToUpdate);

        assertTrue(maybeResult.isPresent());
        final long result = maybeResult.get();

        assertEquals(1, result);

        final List<Vehicle> updatedVehicles = getAsList(collection.find(eq(STATUS_FIELD, statusToUpdate)));
        assertEquals(1, updatedVehicles.size());
    }

    @Test
    public void setAllVehiclesStatus_whenThereAreNoVehicles_ShouldUpdateNothing() {
        final Status statusToUpdate = Status.IN_TREATMENT;

        final Optional<Long> maybeResult = testedVehicleBl.setAllVehiclesStatus(statusToUpdate);

        assertTrue(maybeResult.isPresent());
        final long number = maybeResult.get();
        assertEquals(0, number);

        final List<Vehicle> updatedVehicles = getAsList(collection.find(eq(STATUS_FIELD, statusToUpdate)));
        assertEquals(0, updatedVehicles.size());
    }

    @Test
    public void setAllVehiclesStatus_whenStatusIsInvalid_ShouldUpdateNothing() {
        insertVehicleToDb(NUMBER);
        final Optional<Long> result = testedVehicleBl.setAllVehiclesStatus(null);

        assertFalse(result.isPresent());

        final Optional<Vehicle> maybeVehicleAfterUpdate = Optional.ofNullable(collection.find().first());
        maybeVehicleAfterUpdate
                .map(Vehicle::getStatus)
                .ifPresentOrElse(status -> assertEquals(Status.DONE, status),
                        () -> fail("status should not get updated to null"));
    }

    @Test
    public void removeAllVehicles_whenVehiclesExist_ShouldRemoveAllVehicles() {
        insertVehicleToDb(NUMBER);
        insertVehicleToDb(SECOND_VEHICLE);
        insertVehicleToDb(THIRD_VEHICLE);

        final Optional<Long> maybeDeleteResult = testedVehicleBl.removeAllVehicles();

        maybeDeleteResult.ifPresentOrElse(result -> assertEquals(3, (long) result),
                () -> fail("Should delete 3 vehicles"));

        final List<Vehicle> currentVehicles = getAsList(collection.find());
        assertEquals(0, currentVehicles.size());
    }
}
