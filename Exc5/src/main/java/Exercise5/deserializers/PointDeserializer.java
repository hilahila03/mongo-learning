package Exercise5.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static Exercise5.models.Vehicle.COORDINATES_AMOUNT;
import static Exercise5.models.Vehicle.VEHICLE_LOCATION_TYPE;
import static java.util.stream.StreamSupport.stream;

public class PointDeserializer extends JsonDeserializer<Point> {
    private final static String JSON_KEY_GEOJSON_TYPE = "type";
    private final static String JSON_KEY_GEOJSON_COORDS = "coordinates";
    private final static Double[] DEFAULT_COORDINATES = new Double[]{0.0, 0.0};

    @Override
    public Point deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        final JsonNode tree = parser.getCodec().readTree(parser);
        final String type = tree.get(JSON_KEY_GEOJSON_TYPE).asText();
        final JsonNode coordsNode = tree.get(JSON_KEY_GEOJSON_COORDS);

        List<Double> coordinates = stream(coordsNode.spliterator(), false)
                .map(JsonNode::doubleValue)
                .limit(2)
                .collect(Collectors.toList());

        if(!VEHICLE_LOCATION_TYPE.equalsIgnoreCase(type) || coordsNode.size() != COORDINATES_AMOUNT) {
            coordinates = Arrays.asList(DEFAULT_COORDINATES);
            System.out.println("invalid point");
        }

        return new Point(new Position(coordinates));
    }
}
