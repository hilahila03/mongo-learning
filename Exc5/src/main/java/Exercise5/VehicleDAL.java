package Exercise5;

import Exercise5.models.Status;
import Exercise5.models.Treatment;
import Exercise5.models.UpdateActionResult;
import Exercise5.models.Vehicle;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

public class VehicleDAL {
    public static final String STATUS = "status";
    public static final String NUMBER = "number";
    public static final String MANUFACTURER = "type.manufacturer";
    public static final String PAST_TREATMENTS = "pastTreatments";

    private final MongoCollection<Vehicle> collection;

    public VehicleDAL(MongoCollection<Vehicle> collection) {
        this.collection = collection;
    }

    public void add(Vehicle vehicle) {
        collection.insertOne(vehicle);
    }

    public void add(List<Vehicle> vehicles) {
        collection.insertMany(vehicles);
    }

    public DeleteResult removeAll() {
        return collection.deleteMany(new Document());
    }

    public Vehicle getByNumber(String number) {
        return collection.find(eq(NUMBER ,number)).first();
    }

    public List<Vehicle> getByStatusAndManufacturer(Status status, String manufacturer) {
        return  StreamSupport.stream(
                collection.find(and(eq(STATUS, status), eq(MANUFACTURER, manufacturer))).spliterator(),
                false)
                .collect(Collectors.toList());
    }

    public UpdateResult updateStatus(String number, Status status) {
        return collection.updateOne(eq(NUMBER, number), Updates.set(STATUS, status));
    }

    public UpdateResult updateStatusForAll(Status status) {
        return collection.updateMany(new Document(), Updates.set(STATUS, status));
    }

    public UpdateActionResult addTreatment(String id, Treatment treatment) {
        final UpdateResult result = collection.updateOne(eq(NUMBER, id), Updates.push(PAST_TREATMENTS, treatment));
        return new UpdateActionResult(result.getModifiedCount(), result.getMatchedCount());
    }
}
