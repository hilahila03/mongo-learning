package Exercise5.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum TreatmentType {

    TYRES_REPLACEMENT("tyresReplacement"),
    BRAKES_REPLACEMENT("brakesReplacement"),
    OIL_CHECK("oilCheck"),
    PERIODIC_MAINTENANCE("10000"),
    UNKNOWN("unknown");

    private final String value;

    TreatmentType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue(){
        return value;
    }

    @JsonCreator
    public static TreatmentType fromValue(String value){
        return Arrays.stream(TreatmentType.values())
                .filter(treatment -> treatment.getValue().equals(value))
                .findFirst()
                .orElse(TreatmentType.UNKNOWN);
    }

}
