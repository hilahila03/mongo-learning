package Exercise5.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;
import java.util.Objects;

import static java.util.Objects.*;

@ToString
@Getter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder(builderClassName = "TreatmentBuilder")
public class Treatment {
    public final static TreatmentType DEFAULT_TREATMENT_TYPE = TreatmentType.UNKNOWN;
    private Date date;

    @JsonProperty(value = "treatment_type")
    private TreatmentType treatmentType;

    private int cost;

    public static class TreatmentBuilder {
        public Treatment build() {
            if (isNull(date)) {
                date = new Date();
            }
            if (isNull(treatmentType)) {
                treatmentType = DEFAULT_TREATMENT_TYPE;
            }
            if(cost < 0) {
                cost = 0;
            }

            return new Treatment(date, treatmentType, cost);
        }
    }
}
