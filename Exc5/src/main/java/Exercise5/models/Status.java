package Exercise5.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum Status {
    UNKNOWN("unknown"),
    IN_MISSION("in_mission"),
    IN_TREATMENT("in_treatment"),
    DONE("done");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue(){
        return value;
    }

    @JsonCreator
    public static Status fromValue(String value){
        return Arrays.stream(Status.values())
                .filter(status -> status.getValue().equals(value))
                .findFirst()
                .orElse(Status.UNKNOWN);
    }
}
