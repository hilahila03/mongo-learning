package Exercise5.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder(builderClassName = "TypeBuilder")
public class Type {
    private final static String UNKNOWN_MANUFACTURER = "unknown";

    @JsonProperty(value = "manufacturing_date")
    private Date manufacturingDate;

    @JsonProperty(value = "manufacturer")
    private String manufacturer;

    public static class TypeBuilder {
        public Type build(){
            if (Objects.isNull(manufacturingDate)) {
                manufacturingDate = new Date();
            }
            if (Objects.isNull(manufacturer)) {
                manufacturer = UNKNOWN_MANUFACTURER;
            }
            return (new Type(manufacturingDate, manufacturer));
        }
    }
}
