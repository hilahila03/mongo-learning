package Exercise5.models;

import Exercise5.Exceptions.ValidationFailException;
import Exercise5.deserializers.PointDeserializer;
import Exercise5.serializers.PointSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongodb.client.model.geojson.GeoJsonObjectType;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import lombok.*;
import org.mongojack.ObjectId;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(builderClassName = "VehicleBuilder")
public class Vehicle {
    public static final int VEHICLE_ID_LENGTH = 7;
    public static final String VEHICLE_LOCATION_TYPE = "Point";
    public final static int COORDINATES_AMOUNT = 2;
    public static final int MIN_FUEL_PERCENT = 0;
    public static final int MAX_FUEL_PERCENT = 60;
    public static final Pattern NUMBER_PATTERN = Pattern.compile("^\\d{5,9}");

    @ObjectId
    @EqualsAndHashCode.Exclude
    private String _id;

    private String number;

    private Type type;

    private Status status;

    @JsonDeserialize(using = PointDeserializer.class)
    @JsonSerialize(using = PointSerializer.class)
    private Point currentLocation;

    private Double kilometers;

    @Singular
    private List<Treatment> pastTreatments;

    private Double fuelPercent;

    private static boolean isLocationValid(Point location) {

        final boolean isCoordinatesValid = Optional.ofNullable(location)
                .map(Point::getCoordinates)
                .map(Position::getValues)
                .map(List::size)
                .filter(x -> x == COORDINATES_AMOUNT)
                .isPresent();

        final boolean isLocationTypeValid = Optional.ofNullable(location)
                .map(Point::getType)
                .map(GeoJsonObjectType::getTypeName)
                .filter(x -> x.equals(VEHICLE_LOCATION_TYPE))
                .isPresent();

        return isCoordinatesValid && isLocationTypeValid;
    }

    public static class VehicleBuilder {
        public Vehicle build() {
            if (isNull(number) || !NUMBER_PATTERN.matcher(number).matches()) {
                throw new ValidationFailException(number, "id is invalid!");
            }
            if (isNull(type)) {
                throw new ValidationFailException(number, "Type is required!");
            }
            if (isNull(status)) {
                throw new ValidationFailException(number, "status is invalid!");
            }
            if (isNull(currentLocation) ||
                    (!isLocationValid(currentLocation))) {
                throw new ValidationFailException(number, "location is invalid!");
            }
            if (isNull(kilometers)) {
                throw new ValidationFailException(number, "kilometers is required!");
            }
            if (!isNull(pastTreatments) && (pastTreatments.isEmpty())) {
                throw new ValidationFailException(number, "pastTreatments' list can not be empty!");
            }
            if (isNull(fuelPercent) ||
                    fuelPercent < MIN_FUEL_PERCENT ||
                    fuelPercent > MAX_FUEL_PERCENT) {
                throw new ValidationFailException(number, "fuel percent is invalid");
            }

            return new Vehicle(_id, number, type, status, currentLocation, fuelPercent, pastTreatments, fuelPercent);
        }
    }
}




