package Exercise5.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class
UpdateActionResult {
    private long updated;
    private long matched;
}
