package Exercise5.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.mongodb.client.model.geojson.Point;

import java.io.IOException;
import java.util.List;

import static Exercise5.models.Vehicle.VEHICLE_LOCATION_TYPE;

public class PointSerializer extends JsonSerializer<Point> {

    @Override
    public void serialize(Point point, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        final List<Double> coordinates =  point.getCoordinates().getValues();

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("type", VEHICLE_LOCATION_TYPE);
        jsonGenerator.writeArrayFieldStart("coordinates");
        jsonGenerator.writeNumber(coordinates.get(0));
        jsonGenerator.writeNumber(coordinates.get(1));
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
