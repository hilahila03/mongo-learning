package Exercise5;

import Exercise5.models.Status;
import Exercise5.models.Treatment;
import Exercise5.models.UpdateActionResult;
import Exercise5.models.Vehicle;
import com.mongodb.MongoBulkWriteException;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import lombok.Setter;

import java.util.*;

import static Exercise5.models.Vehicle.NUMBER_PATTERN;
import static java.util.Objects.*;

public class VehicleBL {
    @Setter
    private final VehicleDAL vehicleDal;

    public VehicleBL(VehicleDAL vehicleDAL) {
        this.vehicleDal = vehicleDAL;
    }

    public Optional<Vehicle> findVehicleByNumber(String number) {
        try {
            validateNumber(number);
            return Optional.ofNullable(vehicleDal.getByNumber(number));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

    private void validateNumber(String number) throws IllegalArgumentException {
        if (!isNull(number) && NUMBER_PATTERN.matcher(number).matches()) {
            return;
        }
        throw new IllegalArgumentException("Invalid vehicle id: " + number);
    }

    public void addVehicle(Vehicle vehicle) {
        try {
            validateVehicle(vehicle);
            vehicleDal.add(vehicle);
        } catch (MongoWriteException e) {
            System.out.println("an error occurred during write to Mongo");
            throw e;
        }
    }

    private void validateVehicle(Vehicle vehicle) {
        if (!isNull(vehicle)) {
            return;
        }

        throw new NullPointerException("Vehicle can not be null!");
    }

    private void validateVehicles(Collection<Vehicle> vehicles) {
        if (!isNull(vehicles)) {
            vehicles.forEach(this::validateVehicle);
            return;
        }

        throw new NullPointerException("Vehicle can not be null!");
    }

    public int addVehicles(Collection<Vehicle> vehicles) {
        try {
            validateVehicles(vehicles);
            vehicleDal.add(new ArrayList<>(vehicles));
            return vehicles.size();
        }
        catch (MongoBulkWriteException e) {
            final int insertedAmount = e.getWriteResult().getInsertedCount();
            return insertedAmount;
        }
    }

    public Optional<List<Vehicle>> findVehicleByStatusAndManufacturer(Status status, String manufacturer) {
        try {
            validateStatus(status);
            validateManufacturer(manufacturer);
            return Optional.of(vehicleDal.getByStatusAndManufacturer(status, manufacturer));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

    private void validateManufacturer(String manufacturer) throws IllegalArgumentException {
        if (!isNull(manufacturer) && !(manufacturer.length() < 1)) {
            return;
        }
        throw new IllegalArgumentException("Invalid manufacturer");
    }

    private void validateStatus(Status status) throws IllegalArgumentException{
        if (!isNull(status)) {
            return;
        }
        throw new IllegalArgumentException("status can not be null");
    }

    public Optional<UpdateActionResult> updateStatus(String id, Status status) {
        try {
            validateStatus(status);
            validateNumber(id);
            final UpdateResult result = vehicleDal.updateStatus(id, status);
            return Optional.of(new UpdateActionResult(result.getModifiedCount(), result.getMatchedCount()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

    public Optional<Long> setAllVehiclesStatus(Status status) {
        try {
            validateStatus(status);
            final UpdateResult result = vehicleDal.updateStatusForAll(status);
            return Optional.of(result.getModifiedCount());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

    public Optional<UpdateActionResult> addTreatment(String id, Treatment treatment) {
        try {
        validateNumber(id);
        validateTreatment(treatment);
        return Optional.of(vehicleDal.addTreatment(id, treatment));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

    private void validateTreatment(Treatment treatment) {
        if (!isNull(treatment)) {
            return;
        }
        throw new IllegalArgumentException("status can not be null");
    }

    public Optional<Long> removeAllVehicles() {
        try {
            return Optional.of(vehicleDal.removeAll().getDeletedCount());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }
}
