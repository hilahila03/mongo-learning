package Exercise5.Exceptions;

public class ValidationFailException extends RuntimeException{
    public ValidationFailException(String id, String message) {
        super(String.format("Document with number %s failed validation\r\n%s", id, message));
    }
}
