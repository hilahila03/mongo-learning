package Exercise5;

import Exercise5.models.*;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.w3c.dom.ls.LSOutput;

import java.util.*;
import java.util.stream.StreamSupport;

public class Examples {

    private final VehicleBL vehicleBL;

    static long currNumber = 1935848;

    public Examples(VehicleBL bl) {
        this.vehicleBL = bl;
    }

    public void removeAllVehiclesExample() {
        Optional<Long> maybeResult = vehicleBL.removeAllVehicles();
        maybeResult.ifPresentOrElse(
                deletedCount -> System.out.println(
                        String.format("deleted  %s documents", deletedCount)),
                () -> System.out.println("An error occurred during delete action"));
    }

    public void findByNumberExample() {
        Optional<Vehicle> maybeVehicle = vehicleBL.findVehicleByNumber("1935840");
        maybeVehicle.ifPresentOrElse(
                System.out::println, () -> System.out.println("Vehicle not found for the supplied Id"));
    }

    public void addExample() {
        try {
            vehicleBL.addVehicle(generateVehicle());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addManyExample() {
        final Vehicle[] vehicles = new Vehicle[]{generateVehicle(), generateVehicle(), generateVehicle(), generateVehicle(), generateVehicle(), generateVehicle(), generateVehicle(), generateVehicle(), generateVehicle()};
        int insertedAmount = vehicleBL.addVehicles(Arrays.asList(vehicles));
        System.out.println("inserted: " + insertedAmount + " documents");
    }

    public void addTreatmentExample() {
        Optional<UpdateActionResult> maybeResult =
                vehicleBL.addTreatment("1935819.0",
                        Treatment.builder()
                                .date(new Date())
                                .treatmentType(TreatmentType.OIL_CHECK)
                                .cost(34)
                                .build());

        maybeResult.ifPresentOrElse(result -> System.out.println(
                "updated " + result.getUpdated() + " documents"
        ),
                () -> System.out.println("an error occurred during adding treatment"));
    }

    public void findByStatusAndManufacturerExample() {
        Optional<List<Vehicle>> vehicles = vehicleBL.findVehicleByStatusAndManufacturer(Status.DONE, "Toyota");
        vehicles.ifPresent(vehicleIterable ->
                StreamSupport.stream(vehicleIterable.spliterator(), false).forEach(System.out::println));
    }

    public void updateStatusExample() {
        Optional<UpdateActionResult> maybeResult = vehicleBL.updateStatus("1935841", Status.IN_MISSION);
        maybeResult.ifPresent(result -> System.out.println(
                "updated " + result.getUpdated() + " documents\n" +
                        "matched " + result.getMatched() + " documents"
        ));
    }

    public void updateAllVehiclesStatusExample() {
        Optional<Long> maybeResult = vehicleBL.setAllVehiclesStatus(Status.IN_MISSION);
        maybeResult.ifPresentOrElse(
                updatedCount -> System.out.println(
                        "updated " + updatedCount + " documents"
                ),
                () -> System.out.println("an Error occurred during update"));
    }

    private Vehicle generateVehicle() {
        Double[] coordinates = {30.0, 30.0};
        Point location = new Point(new Position(Arrays.asList(coordinates)));
        Treatment[] treatments = new Treatment[]{
                (Treatment.builder().date(new Date()).treatmentType(TreatmentType.BRAKES_REPLACEMENT).cost(20)).build(),
                (Treatment.builder().date(new Date()).treatmentType(TreatmentType.TYRES_REPLACEMENT).cost(50)).build()
        };

        Vehicle.VehicleBuilder vehicleBuilder = Vehicle.builder();
        vehicleBuilder.
                number(String.valueOf(++currNumber)).
                currentLocation(location).
                type(Type.builder().manufacturingDate(new Date()).manufacturer("Toyota").build()).
                status(Status.DONE).
                fuelPercent(40.0).
                kilometers(15.0).
                pastTreatments(Arrays.asList(treatments));

        return vehicleBuilder.build();
    }

}
