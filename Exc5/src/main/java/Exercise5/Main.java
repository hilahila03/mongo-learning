package Exercise5;

import Exercise5.models.Vehicle;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.bson.UuidRepresentation;
import org.mongojack.JacksonMongoCollection;

public class Main {

    private final static MongoClient client = MongoClients.create();

    private final static MongoCollection<Vehicle> collection =
            JacksonMongoCollection.builder().build(client,
                    "hafifa",
                    "Vehicles",
                    Vehicle.class,
                    UuidRepresentation.JAVA_LEGACY);

    private final static Examples examples = new Examples(new VehicleBL(new VehicleDAL(collection)));

    public static void main(String[] args) {

//        examples.addExample();
//
//        examples.addManyExample();
//
        examples.findByNumberExample();
//
//        examples.findByStatusAndManufacturerExample();
//
//        examples.updateStatusExample();
//
//        examples.updateAllVehiclesStatusExample();
//
//        examples.removeAllVehiclesExample();
//
//        examples.addTreatmentExample();
    }
}
