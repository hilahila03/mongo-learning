db.createCollection("Vehicles", {
    validator: {
        $jsonSchema: {
            required: ["type", "status", "current_location", "kilometers"],
            properties: {
                status: {
                    enum: ["done", "in_treatment", "in_mission"]
                },
                past_treatments: {
                    type: "array",
                    minItems: 1,
                    items: {
                        properties: {
                            treatment_type: {
                                enum: ["tyres_replacement", "10000", "brakes_replacement", "oil_check"]
                            }
                        }
                    }
                }
            }
        }
    },
}
);


db.Vehicles.insertMany([
    {
        _id: 85423659,
        type: {
            manufacturing_date: ISODate("2009-03-21"),
            manufacturer: "Toyota"
        },
        status: "in_mission",
        current_location: {
            type: "Point",
            coordinates: [34, 35]
        },
        kilometers: 3950,
        past_treatments: [
            {
                date: ISODate("2012-03-21"),
                treatment_type: "10000",
                cost: 12000
            },
            {
                date: ISODate("2015-08-01"),
                treatment_type: "tyres_replacement",
                cost: 2700
            }
        ]
    },
    {
        _id: 12365985,
        type: {
            manufacturing_date: ISODate("2012-04-21"),
            manufacturer: "Mazda"
        },
        status: "in_treatment",
        current_location: {
            type: "Point",
            coordinates: [32, 35]
        },
        kilometers: 10236,
        past_treatments: [
            {
                date: ISODate("2015-12-21"),
                treatment_type: "10000",
                cost: 10000
            },
            {
                date: ISODate("2017-03-14"),
                treatment_type: "brakes_replacement",
                cost: 12300
            }
        ]
    },
    {
        _id: 14562354,
        type: {
            manufacturing_date: ISODate("2014-03-21"),
            manufacturer: "BMW"
        },
        status: "done",
        current_location: {
            type: "Point",
            coordinates: [32.5, 35.5]
        },
        kilometers: 11203,
        past_treatments: [
            {
                date: ISODate("2014-12-21"),
                treatment_type: "oil_check",
                cost: 1780
            }
        ]
    },
    {
        _id: 15623587,
        type: {
            manufacturing_date: ISODate("2007-03-21"),
            manufacturer: "Opel"
        },
        status: "in_mission",
        current_location: {
            type: "Point",
            coordinates: [32, 35]
        },
        kilometers: 7560,
        past_treatments: [
            {
                date: ISODate("2009-03-21"),
                treatment_type: "tyres_replacement",
                cost: 6500
            },
            {
                date: ISODate("2009-03-21"),
                treatment_type: "brakes_replacement",
                cost: 8000
            }
        ]
    },
    {
        _id: 45876259,
        type: {
            manufacturing_date: ISODate("2009-07-03"),
            manufacturer: "Mazda"
        },
        status: "done",
        current_location: {
            type: "Point",
            coordinates: [32, 34.5]
        },
        kilometers: 3000,
    },
    {
        _id: 98564569,
        type: {
            manufacturing_date: ISODate("2011-03-21"),
            manufacturer: "Opel"
        },
        status: "in_mission",
        current_location: {
            type: "Point",
            coordinates: [32.5, 35]
        },
        kilometers: 7856,
        past_treatments: [
            {
                date: ISODate("2013-03-21"),
                treatment_type: "10000",
                cost: 12000
            },
            {
                date: ISODate("2014-03-21"),
                treatment_type: "tyres_replacement",
                cost: 2700
            }
        ]
    },
    {
        _id: 48799878,
        type: {
            manufacturing_date: ISODate("2009-04-21"),
            manufacturer: "Mazda"
        },
        status: "in_treatment",
        current_location: {
            type: "Point",
            coordinates: [32, 35]
        },
        kilometers: 15623,
        past_treatments: [
            {
                date: ISODate("2011-03-21"),
                treatment_type: "brakes_replacement",
                cost: 12000
            },
            {
                date: ISODate("2012-03-21"),
                treatment_type: "tyres_replacement",
                cost: 2700
            },
            {
                date: ISODate("2013-03-21"),
                treatment_type: "10000",
                cost: 13456
            },
            {
                date: ISODate("2012-06-21"),
                treatment_type: "oil_check",
                cost: 1780
            }
        ]
    },
    {
        _id: 45696555,
        type: {
            manufacturing_date: ISODate("2011-06-21"),
            manufacturer: "Toyota"
        },
        status: "in_mission",
        current_location: {
            type: "Point",
            coordinates: [33, 35]
        },
        kilometers: 7999,
        past_treatments: [
            {
                date: ISODate("2013-03-21"),
                treatment_type: "10000",
                cost: 12000
            },
            {
                date: ISODate("2013-07-21"),
                treatment_type: "tyres_replacement",
                cost: 2700
            }
        ]
    },
    {
        _id: 45656789,
        type: {
            manufacturing_date: ISODate("2016-01- 21"),
            manufacturer: "Opel"
        },
        status: "done",
        current_location: {
            type: "Point",
            coordinates: [32, 35]
        },
        kilometers: 7895,
    },
    {
        _id: 23656987,
        type: {
            manufacturing_date: ISODate("2017-03-21"),
            manufacturer: "Mazda"
        },
        status: "in_treatment",
        current_location: {
            type: "Point",
            coordinates: [32, 34]
        },
        kilometers: 2451,
    },
    {
        _id: 36598564,
        type: {
            manufacturing_date: ISODate("2019-03-21"),
            manufacturer: "Opel"
        },
        status: "done",
        current_location: {
            type: "Point",
            coordinates: [32, 35]
        },
        kilometers: 4562,
    }
]);
