// Search

// 1 
db.Vehicles.find({ _id: 23656987 });

// 2
db.Vehicles.find({ status: "in_mission" }, { current_location: 1 });

// 3
db.Vehicles.find({ status: { $ne: "in_mission" } }, { _id: 0, current_location: 1, "type.manufacturing_date": 1 });

// 4
db.Vehicles.find().sort({ "type.manufacturing_date": 1 }).limit(3);

// 5
db.Vehicles.aggregate([
    { $sort: { "type.manufacturing_date": -1 } },
    { $limit: 3 }
]);

// 6
db.Vehicles.find({ "type.manufacturing_date": { $gt: new Date(2010, 1, 1) } });

//7
db.Vehicles.find({ past_treatments: { $exists: true } });

//8
db.Vehicles.find({ "past_treatments.2": { $exists: true } });


// Update 

// 1
db.Vehicles.updateMany({}, {
    $set: { fuel_amount: 60 }
});

// 2

db.Vehicles.updateMany(
    { status: "in_mission" },
    { $inc: { fuel_amount: -35 } }
);

// 3

db.runCommand({
    collMod: 'Vehicles',
    validator: {
        $and:
            [
                { fuel_amount: { $gte: 0 } },
                { fuel_amount: { $lte: 60 } }
            ]
    }
});

db.Vehicles.updateMany(
    { fuel_amount: { $lt: 40 } },
    { $inc: { fuel_amount: +20 } }
);

// 4

db.runCommand({
    collMod: 'Vehicles',
    validator: {
        $jsonSchema: {
            required: ["type", "status", "current_location", "kilometers"],
            properties: {
                status: {
                    enum: ["done", "in_treatment", "in_mission"]
                },
                past_treatments: {
                    type: "array",
                    minItems: 1,
                    items: {
                        properties: {
                            treatment_type: {
                                enum: ["tyres_replacement", "10000", "brakes_replacement", "oil_check"]
                            }
                        }
                    }
                }
            }
        }
    }
});

db.Vehicles.updateMany(
    {},
    {
        $rename: { 'fuel_amount': 'fuel_percent' }
    }
);

db.Vehicles.updateMany(
    {},
    {
        $mul: { 'fuel_percent': 1.6666 }
    }
);

// 5

db.Vehicles.updateMany(
    { status: "in_treatment" },
    {
        $push: {
            past_treatments: {
                $each: [
                    {
                        date: ISODate(),
                        treatment_type: "tyres_replacement",
                        cost: 1000
                    },
                    {
                        date: ISODate(),
                        treatment_type: "brakes_replacement",
                        cost: 1200
                    }
                ]
            }
        }
    }
);

// 6

db.Vehicles.updateMany({ "past_treatments.1": { $exists: false } }, { $unset: { past_treatments: "" } });


// first way 
db.Vehicles.aggregate([
    { "$unwind": "$past_treatments" },
    { "$group": { '_id': '$_id', 'minCost': { '$min': "$past_treatments.cost" } } }
    ]).forEach(function (vehicle) {
        db.Vehicles.update(
            { '_id': vehicle._id },
            { '$pull': { 'past_treatments': { 'cost': vehicle.minCost } } }
        )
    }
);

// second way 
db.Vehicles.updateMany(
    { past_treatments: { $exists: true } },
    {
        $push: {
            past_treatments: {
                $each: [],
                $sort: { date: 1 }
            }
        }
    }
);

db.Vehicles.updateMany({ "past_treatments.1": { $exists: true } }, { $pop: { past_treatments: -1 } });

// 7

db.Vehicles.updateMany(
    { past_treatments: { $exists: true } },
    {
        $push: {
            past_treatments: {
                $each: [],
                $sort: { cost: -1 }
            }
        }
    }
);

// 8

db.Vehicles.update(
    { _id: 55555555 },
    {
        _id: 55555555,
        type: {
            manufacturing_date: ISODate(),
            manufacturer: "Opel"
        },
        status: "done",
        current_location: {
            type: "Point",
            coordinates: [30, 33]
        },
        kilometers: 0,
        fuel_percent: 78
    },
    {
        upsert: true
    }
);


