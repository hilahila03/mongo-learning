db.getCollection("Vehicles").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$unwind: "$past_treatments"
		},

		// Stage 2
		{
			$group: { 
			                "_id" : "$status", 
			                "total_treatments_cost" : { 
			                    "$sum" : "$past_treatments.cost"
			                }, 
			                "vehicles" : { 
			                    "$addToSet" : "$$ROOT"
			                }
			            }
		},

		// Stage 3
		{
			$match: { 
			                "total_treatments_cost" : { 
			                    "$gt" : 10000.0
			                }
			            }
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
