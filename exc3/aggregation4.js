db.getCollection("Vehicles").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$sort: {
			      kilometers: 1
			}
		},

		// Stage 2
		{
			$group: {
			    _id: "$type.manufacturer",
			    min_vehicle: { $first:  "$$ROOT" },
			    max_vehicle: { $last:  "$$ROOT" }
			    }
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
