db.getCollection("Vehicles").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$project: {
			    year: { $year: "$type.manufacturing_date" },
			    month: { $month: "$type.manufacturing_date" },
			    number: "$_id"
			}
		},

		// Stage 2
		{
			$match: {
			    month: { $gt: 6, $lt: 9}
			}
		},

		// Stage 3
		{
			$project: {
			    month: 0,   
			    _id: 0
			}
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
