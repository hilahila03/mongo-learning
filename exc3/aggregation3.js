db.Vehicles.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$unwind: {
			      path: "$past_treatments"
			}
		},

		// Stage 2
		{
			$replaceRoot: {
			    newRoot: "$past_treatments"
			}
		},

		// Stage 3
		{
			$match: {
			    treatment_type: "oil_check"
			}
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
