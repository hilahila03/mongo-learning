db.getCollection("Vehicles").aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$bucket: {
			        groupBy: "$type.manufacturing_date",
			        boundaries: [ISODate("2007-01-01"), ISODate("2008-01-01"), ISODate("2009-01-01"), 
			                     ISODate("2010-01-01"), ISODate("2011-01-01"), ISODate("2012-01-01"),
			                     ISODate("2013-01-01"), ISODate("2014-01-01"), ISODate("2015-01-01"), 
			                     ISODate("2016-01-01"), ISODate("2017-01-01"), ISODate("2018-01-01"),
			                     ISODate("2019-01-01"), ISODate("2020-01-01"),ISODate("2021-01-01")],
			        default: "Other",
			        output: {
			            "avarage_distance" : { $avg: "$kilometers"}
			        }
			    }
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
